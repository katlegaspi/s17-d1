// Common examples of arrays
let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];
let room = ['401A', '401B', '401C'];
// NOT recommended use of array
let mixedArr = [12, 'Asus', null, undefined, {}];

// Alternative way to write arrays
let myTasks = [
	'drink HTML',
	'eat Javascript',
	'inhale CSS',
	'bake SASS'
];
// index = n - 1 where n is the total length of array

// Reassigning array values
console.log('Array before reassignment');
console.log(myTasks);
myTasks[0] = 'clean node';
console.log('Array after reassignment');
console.log(myTasks);

// Reading from Array: name of array and index

console.log(grades[0]);
console.log(computerBrands[3]);
// Accessing an array element that does not exist
console.log(grades[20]);

// Getting the length of an array (.length). Measures total # of elements

console.log(computerBrands.length);

// Manipulating Arrays
// Array Methods
// Mutator Methods - functions that mutate or change an array after they're created

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

// push() - adds an element in the end of an array and returns the array's length.
console.log('Current Array');
console.log(fruits);
let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log('Mutated array from push method.');
console.log(fruits);

// Adding multiple elements to an array
fruits.push('Avocado', 'Guava');
console.log('Mutated array from push method.');
console.log(fruits);

// pop() - removes the last element in an array and returns the removed element
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log('Mutated array from pop method.');
console.log(fruits);

// unshift() - adds one or more elements at the beginning of an array
/* Syntax:
	arrayName.unshift('elementA');
	arrayName.unshift('elementA', 'elementB');
*/

fruits.unshift('Lime', 'Banana');
console.log('Mutated array from unshift method.');
console.log(fruits);

// shift() - removes an element at the beginning of an array and returns the removed element. Syntax: arrayName.shift();

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated array from shift method.');
console.log(fruits);

// splice() - simultaneously removes elements from a specified index number and adds elements. Syntax: arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);

fruits.splice(1, 2, 'Lime', 'Cherry');
console.log('Mutated array from splice method.');
console.log(fruits);

// sort() - rearranges the array elements in alphanumeric order. Syntax: arrayName.sort();

fruits.sort();
console.log('Mutated array from sort method.');
console.log(fruits);

// reverse() - reverses the order of array elements

fruits.reverse();
console.log('Mutated array from reverse method.');
console.log(fruits);

// Non-mutator methods - functions that do not modify or change an array after they're created

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

// indexOf() - returns the index number of the first matching element found in an array
// Syntax: arrayName.indexOf(searchValue);
let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf method: ' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf method: ' + invalidCountry);

// lastIndexOf() - returns the index number of the last	 matching element found in an array. Syntax: arrayName.lastIndexOf(searchValue);

// Getting the index number starting from the last element
let lastIndex = countries.lastIndexOf('PH')
console.log('Result of lastIndexOf method: ' + lastIndex);

// slice() - portions/slices elements from an array and returns a new array
/* Syntax:
	arrayName.slice(startingIndex);
	arrayName.slice(startingIndex, lengthCount);
*/
console.log(countries);
// Slicing of element from a specified index to another index
let slicedArrayA = countries.slice(2);
console.log('Result of slice method:');
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 6);
console.log('Result of slice method:');
console.log(slicedArrayB);

// toString() - returns an array as a string separated by commas. Syntax: arrayName.toString();
let stringArray = countries.toString();
console.log('Result from toString method:');
console.log(stringArray);

// concat() - combines two arrays and returns the combined result. Syntax: arrayA.concat(arrayB);

let tasksArrayA = ['drink HTMl', 'eat Javascript'];
let tasksArrayB = ['inhale CSS', 'breathe SASS'];
let tasksArrayC = ['get Git', 'be node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log('Result from concat method:');
console.log(tasks);

// combining multiple arrays
console.log('Result from concat method:');
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

// combining arrays with elements
let combinedTasks = tasksArrayA.concat('smell Express', 'throw React');
console.log('Result from concat method:');
console.log(combinedTasks);

// join() - returns an array as a string separated by specified separator string. Syntax: arrayName.join('separator string');

let users = ['John', 'Jane', 'Joe', 'Robert'];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));

// Iteration methods - these are loops designed to perform repetitive tasks on arrays
// - common practice to use the singular form of the array content for parameter names used in array loops

// forEach() - similar to a for loop that iterates on each array element
/* Syntax:
	arrayName.forEach(function(indivElement){
		statement;
	}
*/
console.log('Result from forEach method:');
allTasks.forEach(function(task){
	console.log(task);
});

// Using forEach with conditional statements
let filteredTasks = [];

allTasks.forEach(function(task){
	if(task.length > 10){
		filteredTasks.push(task)
	}
});
console.log('Result of filtered tasks:');
console.log(filteredTasks);

// map() - iterates on each element and returns a new array with different values depending on the result of the function's operation.
/* Syntax:
	let/const resultArray = arrayName.map(function(indivElement){
		statement/s;
	})
*/

let numbers = [1, 2, 3, 4, 5];
let numberMap = numbers.map(function(number){
	return number * number;
})

console.log('Result of map method:');
console.log(numberMap);

// every() - checks if all elements in an array meet the given condition
/* Syntax:
	let/const resultArray = arrayName.every(function(indivElement){
		return expression/condition;
	})
*/

let allValid = numbers.every(function(number){
	return (number < 3);
});
console.log('Result of every method:');
console.log(allValid);

// some() - check if at least one element in the array meets the given condition
/* Syntax:
	let/const resultArray = arrayName.some(function(indivElement){
		return expression/condition;
	})
*/

let someValid = numbers.some(function(number){
	return (number < 2);
});
console.log('Result of some method:');
console.log(someValid);

// filter() - return a new array that contains elements which meets the given condition
/* Syntax:
	let/const resultArray = arrayName.filter(function(indivElement){
		return expression/condition;
	})
*/

let filterValid = numbers.filter(function(number){
	return (number < 3);
});
console.log('Result of filter method:');
console.log(filterValid);

let nothingFound = numbers.filter(function(number){
	return (number == 0);
});
console.log('Result of filter method:');
console.log(nothingFound);

// Fitering using forEach

let filteredNumbers = [];

numbers.forEach(function(number){
	if(number > 3){
		filteredNumbers.push(number);
	}
});
console.log('Result of filter method:');
console.log(filteredNumbers);

// includes() - the result of the first method is used on the second method until all "chained" methods have been resolved.

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes('a');
});
console.log(filteredProducts);

// reduce() - evaluates elements from left to right and either returns/reduces the array into a single value
/* Syntax:
	let/const resultArray = arrayName.reduce(function(accumulator, currentValue){
		return expression/operation
	});
*/

let iteration = 0;

let reducedArray = numbers.reduce(function(x, y){
	console.warn('current iteration: ' + ++iteration);
	console.log('accumulator: ' + x);
	console.log('currentValue: ' + y);

	return x + y;
});
console.log("Result of reduce method: " + reducedArray);

// Reducing string arrays
let list = ['Hello', 'Again', 'World'];
let reducedJoin = list.reduce(function(x, y){
	return x + ' ' + y;
});
console.log("Result of reduce method: " + reducedJoin);

// Multidimensional Array
// Two dimensional Array - having an array within an array

let oneDim = [];
//1st dim       0       1
//2nd dim      0  1    0  1
let twoDim = [[2, 4], [6, 8]];
// 2x2 two dimensional array
console.log(twoDim[1][0]);
// logic: arrayName[dim1 index][dim2 index];
console.log(twoDim[0][1]);
console.log(twoDim[1][1]);

// 3x2 two dimensional array
//               0       1        2
//              0  1    0  1    0   1
let twoDim2 = [[2, 4], [6, 8], [10, 12]];
console.log(twoDim2[2][0]);